import bpy
from . import ops_generate
from . import ops_game
from . import ops_actions

bl_info = {
    'name': 'Game rig generation',
    'author': 'Xin',
    'version': (0, 1),
    'blender': (2, 93, 0),
    'location': '3d view > N panel > GR',
    'description': 'Operators for generating game rigs based on CGDive videos',
    "doc_url": "https://gitlab.com/x190/game_rig_generation",
    "tracker_url": "https://gitlab.com/x190/game_rig_generation",
    'category': 'Rigging'
}


class ActionListEntry(bpy.types.PropertyGroup):
    enabled: bpy.props.BoolProperty(name="Enabled", default=False)
    frame_start: bpy.props.IntProperty(name="Start frame", subtype='UNSIGNED', min=0)
    frame_end: bpy.props.IntProperty(name="End frame", subtype='UNSIGNED', min=0)
    range_type: bpy.props.EnumProperty( name="Frame range",
                                        items = ( ("LOOP", "Loop", ""),
                                                  ("ALL", "All", ""),
                                                  ("CUSTOM", "Custom", ""),
                                                 ),
                                        default= 'ALL',
                                        description = "Frame range" )

class ActionList(bpy.types.PropertyGroup):
    index: bpy.props.IntProperty(default=-1, min=-1, name="Active action")
    entries: bpy.props.CollectionProperty(type=ActionListEntry)

class CGDiveProperties(bpy.types.PropertyGroup):
    source_rig:     bpy.props.StringProperty(name="Original rig", description="Original rig")
    control_rig:    bpy.props.StringProperty(name="Control rig", description="Control rig")
    game_rig:       bpy.props.StringProperty(name="Game rig", description="Game rig")

    action_list:    bpy.props.PointerProperty(type=ActionList)

class AddonPanel:
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "GR"

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT' or context.mode == 'POSE'

class PANEL_PT_CGDivePanel(AddonPanel, bpy.types.Panel):
    bl_idname = "PANEL_PT_CGDivePanel"
    bl_label = "Generate rigs"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        cgdive_settings = context.scene.cgdive_settings

        row = layout.row()
        row.prop_search(cgdive_settings, "source_rig", context.scene, "objects", icon='OUTLINER_OB_ARMATURE')
        row = layout.row()
        row.prop_search(cgdive_settings, "control_rig", context.scene, "objects", icon='OUTLINER_OB_ARMATURE')
        row = layout.row()
        row.prop_search(cgdive_settings, "game_rig", context.scene, "objects", icon='OUTLINER_OB_ARMATURE')

        row = layout.row()
        row.operator(ops_generate.GenerateControlRig.bl_idname, icon='OUTLINER_OB_ARMATURE')
        row = layout.row()
        row.operator(ops_generate.GenerateGameRig.bl_idname, icon='OUTLINER_OB_ARMATURE')


class PANEL_PT_GameRigPanel(AddonPanel, bpy.types.Panel):
    bl_idname = "PANEL_PT_GameRigPanel"
    bl_label = "Game rig"

    def draw(self, context):
        return

class PANEL_PT_GameRigPanelConstraints(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_GameRigPanel"
    bl_idname = "PANEL_PT_GameRigPanelConstraints"
    bl_label = "Constraints"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False

        row = layout.row()
        row.operator(ops_game.ConstraintGameRig.bl_idname, icon='CONSTRAINT_BONE')
        row = layout.row()
        row.operator(ops_game.DisableConstraintGameRig.bl_idname, icon='CONSTRAINT_BONE')
        row.operator(ops_game.EnableConstraintGameRig.bl_idname, icon='CONSTRAINT_BONE')


class PANEL_PT_GameRigPanelSquash(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_GameRigPanel"
    bl_idname = "PANEL_PT_GameRigPanelSquash"
    bl_label = "Squash/Stretch options"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False

        row = layout.row()
        row.operator(ops_game.FlattenGameRig.bl_idname, icon='OUTLINER_DATA_ARMATURE')
        row = layout.row()
        row.operator(ops_game.DisconnectGameRig.bl_idname, icon='OUTLINER_DATA_ARMATURE')


class CUSTOM_UL_actions(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        # ~ self.use_filter_show = False
        r = layout.row()
        split = r.split(factor=0.12)
        split.prop(item, "enabled", text="", emboss=True, translate=False)
        split = split.split(factor=0.15)
        split.label(text=item.range_type, translate=False)
        split.label(text=item.name, translate=False)


class PANEL_PT_GameRigPanelBake(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_GameRigPanel"
    bl_idname = "PANEL_PT_GameRigPanelBake"
    bl_label = "Bake Actions"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        accl = context.scene.cgdive_settings.action_list

        box = layout.box()
        row = box.row(align=True)
        row.label(text="Control rig actions")

        row.operator(ops_actions.UpdateActionsList.bl_idname, icon='FILE_REFRESH', text="")
        row.separator()
        row.operator(ops_actions.ActionsDisableAll.bl_idname, icon='CHECKBOX_DEHLT', text="")
        row.operator(ops_actions.ActionsEnableAll.bl_idname, icon='CHECKMARK', text="")
        row.separator()
        row.operator(ops_actions.ResetActionsList.bl_idname, icon='IMPORT', text="")

        row = box.row()
        row.template_list( "CUSTOM_UL_actions", "",
                            accl, "entries",
                            accl, "index",
                            rows=2, maxrows=8 )

        if accl.index >= 0:
            entry = accl.entries[accl.index]
            row = box.row()
            row.prop(entry, "range_type")
            row = box.row()
            row.use_property_split = False
            if entry.range_type == 'CUSTOM':
                c = row.column()
                c.prop(entry, "frame_start", text="Start", emboss=True)
                c = row.column()
                c.prop(entry, "frame_end", text="End", emboss=True)
                row = box.row()
                c = row.column()
                c.operator(ops_actions.ActionsSetRange.bl_idname)

        row = layout.row()
        row.operator(ops_actions.BakeActionsGameRig.bl_idname, icon='ACTION')


classes = (
    ActionListEntry,
    ActionList,
    CGDiveProperties,
    ops_generate.GenerateGameRig,
    ops_generate.GenerateControlRig,
    ops_game.ConstraintGameRig,
    ops_game.FlattenGameRig,
    ops_game.DisconnectGameRig,
    ops_game.EnableConstraintGameRig,
    ops_game.DisableConstraintGameRig,
    ops_actions.UpdateActionsList,
    ops_actions.BakeActionsGameRig,
    ops_actions.ActionsEnableAll,
    ops_actions.ActionsDisableAll,
    ops_actions.ActionsSetRange,
    ops_actions.ResetActionsList,
    CUSTOM_UL_actions,
    PANEL_PT_CGDivePanel,
    PANEL_PT_GameRigPanel,
    PANEL_PT_GameRigPanelConstraints,
    PANEL_PT_GameRigPanelSquash,
    PANEL_PT_GameRigPanelBake,
)

def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.types.Scene.cgdive_settings = bpy.props.PointerProperty(type=CGDiveProperties)

def unregister():
    del bpy.types.Scene.cgdive_settings
    for c in reversed(classes):
        bpy.utils.unregister_class(c)

if __name__ == "__main__":
    register()

# https://youtu.be/8PSj_1-E9uY?list=PLdcL5aF8ZcJvCyqWeCBYVGKbQgrQngen3&t=838 stretch end too
