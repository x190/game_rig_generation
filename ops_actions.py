import bpy
from . import ops_generate
from . import utils
from bpy_extras import anim_utils


class UpdateActionsList(ops_generate.BaseOperator):
    """Update list with control rig actions"""
    bl_idname = "cgdive.update_action_list"
    bl_label = "Update"

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT' or context.mode == 'POSE'

    def execute(self, context):
        if not self.check_rigs(context, [1]):
            return {'CANCELLED'}

        actions = utils.get_rig_actions(self.control_rig)
        accl = context.scene.cgdive_settings.action_list
        active_acc = None
        if accl.index >= 0 and accl.index < len(accl.entries):
            active_acc = accl.entries[accl.index].name

        entries = []
        for a in actions:
            entry = accl.entries.get(a, None)
            if entry is not None:
                enabled = entry.enabled
                frame_start = entry.frame_start
                frame_end = entry.frame_end
                range_type = entry.range_type
            else:
                enabled = False
                acc = bpy.data.actions[a]
                frame_start = int(acc.frame_range[0])
                frame_end = int(acc.frame_range[1])
                range_type = 'ALL'
            entries.append((a, enabled, frame_start, frame_end, range_type))

        accl.entries.clear()
        accl.index = -1

        for i, e in enumerate(entries):
            entry = accl.entries.add()
            entry.name = e[0]
            if active_acc is not None \
               and entry.name == active_acc:
                   accl.index = i
            entry.enabled = e[1]
            entry.frame_start = e[2]
            entry.frame_end = e[3]
            entry.range_type = e[4]

        return {'FINISHED'}

class ResetActionsList(ops_generate.BaseOperator):
    """Reset list with control rig actions"""
    bl_idname = "cgdive.reset_action_list"
    bl_label = "Reset"

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT' or context.mode == 'POSE'

    def execute(self, context):
        accl = context.scene.cgdive_settings.action_list
        accl.entries.clear()
        accl.index = -1
        if not self.check_rigs(context, [1]):
            return {'FINISHED'}

        actions = utils.get_rig_actions(self.control_rig)
        for a in actions:
            acc = bpy.data.actions[a]
            entry = accl.entries.add()
            entry.name = a
            entry.enabled = False
            entry.frame_start = int(acc.frame_range[0])
            entry.frame_end = int(acc.frame_range[1])
            entry.range_type = 'ALL'

        return {'FINISHED'}

class ActionsDisableAll(bpy.types.Operator):
    bl_idname = "cgdive.actions_disable_all"
    bl_label = "Disable all actions"
    bl_description = "Disable all actions"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        accl = context.scene.cgdive_settings.action_list
        for e in accl.entries:
            e.enabled = False
        return {"FINISHED"}


class ActionsEnableAll(bpy.types.Operator):
    bl_idname = "cgdive.actions_enable_all"
    bl_label = "Enable all actions"
    bl_description = "Enable all actions"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        accl = context.scene.cgdive_settings.action_list
        for e in accl.entries:
            e.enabled = True
        return {"FINISHED"}


class ActionsSetRange(bpy.types.Operator):
    bl_idname = "cgdive.actions_set_range"
    bl_label = "Set full range"
    bl_description = "Set full range"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        accl = context.scene.cgdive_settings.action_list
        index = accl.index

        e = accl.entries[index]
        acc = bpy.data.actions.get(e.name, None)
        if acc is not None:
            e.frame_start = int(acc.frame_range[0])
            e.frame_end = int(acc.frame_range[1])
            return {"FINISHED"}
        else:
            self.report({'ERROR'}, "Action '{0}' not found.".format(e.name))
            return {'CANCELLED'}


class BakeActionsGameRig(ops_generate.BaseOperator):
    """Bake control rig actions onto the game rig"""
    bl_idname = "cgdive.bake_actions_game_rig"
    bl_label = "Bake actions"

    rename_mode: bpy.props.EnumProperty( name = "Renaming",
                                         items = ( ('NONE', "None", ""),
                                                   ('PREFIX', "Prefix", ""),
                                                   ('SUFFIX', "Suffix", ""),
                                                   ('REPLACE', "Replace", ""),
                                                 ),
                                         default = 'NONE' )
    text1: bpy.props.StringProperty(default="")
    text2: bpy.props.StringProperty(default="")
    postdisable_constraints: bpy.props.BoolProperty(name="Disable constraints after bake", default=True)

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT' or context.mode == 'POSE'

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        layout.prop(self, "rename_mode")
        if self.rename_mode == 'PREFIX':
            layout.prop(self, "text1", text="Prefix")
        elif self.rename_mode == 'SUFFIX':
            layout.prop(self, "text1", text="Suffix")
        elif self.rename_mode == 'REPLACE':
            layout.prop(self, "text1", text="Find")
            layout.prop(self, "text2", text="Replace")
        layout.prop(self, "postdisable_constraints")

    def get_action_name(self, name):
        if self.rename_mode == 'NONE':
            return name
        if self.rename_mode == 'REPLACE':
            return name.replace(self.text1, self.text2)
        elif self.rename_mode == 'PREFIX':
            return self.text1 + name
        elif self.rename_mode == 'SUFFIX':
            return name + self.text1
        raise ValueError("Invalid rename_mode: '{}'".format(self.rename_mode))

    def execute(self, context):
        if not self.check_rigs(context, [1, 2]):
            return {'CANCELLED'}

        utils.nla_tracks_set(self.control_rig, solo=False, mute=True)
        utils.nla_tracks_set(self.game_rig, solo=False)
        utils.set_mute_all_constraints(self.game_rig, False)

        control_acc0 = utils.get_active_action(self.control_rig)
        game_acc0 = utils.get_active_action(self.game_rig)

        settings = context.scene.cgdive_settings
        accl = settings.action_list
        bake_to_objects = [[self.game_rig, None]]
        baked_actions = []
        for e in accl.entries:
            if not e.enabled:
                continue
            acc = bpy.data.actions.get(e.name, None)
            if acc is None:
                print("Action '{}' not found. Baking skipped.".format(e.name))
                continue

            self.control_rig.animation_data.action = acc
            if e.range_type == 'CUSTOM':
                range_start = e.frame_start
                range_end = e.frame_end
            else:
                range_start = int(acc.frame_range[0])
                range_end = int(acc.frame_range[1])
                if e.range_type == 'LOOP':
                    if range_start != range_end:
                        range_end -= 1
            frames = [ i for i in range(range_start, range_end+1) ]

            acc_baked = anim_utils.bake_action_objects( bake_to_objects,
                                                        frames=frames,
                                                        only_selected=False,
                                                        do_pose=True,
                                                        do_object=False,
                                                        do_visual_keying=True,
                                                        do_constraint_clear=False,
                                                        do_parents_clear=False,
                                                        do_clean=False )

            acc_baked = acc_baked[0]
            acc_baked.name = self.get_action_name(acc.name)
            new_track = self.game_rig.animation_data.nla_tracks.new()
            new_track.name = acc_baked.name
            new_track.strips.new(acc_baked.name, frames[0], acc_baked)
            new_track.mute = True
            new_track.is_solo = False
            baked_actions.append(acc_baked.name)

        utils.set_active_action(self.game_rig, game_acc0)
        utils.set_active_action(self.control_rig, control_acc0)

        if self.postdisable_constraints:
            utils.set_mute_all_constraints(self.game_rig, True)

        self.report({'INFO'}, "{0} actions baked on game rig.".format(len(baked_actions)))
        if len(baked_actions) > 0:
            print("\nBaked actions from control rig '{}' onto game rig '{}':".format(self.control_rig.name, self.game_rig.name))
            for a in baked_actions:
                print("  {}".format(a))
            print()
        return {'FINISHED'}
