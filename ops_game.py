import bpy
from . import ops_generate
from . import utils

class GameRigOperator(ops_generate.BaseOperator):
    selection_type: bpy.props.EnumProperty( name = "Bone selection",
                                             items = ( ('ALL',  "All", ""),
                                                       ('SELECTED', "Only selected", "")
                                                      ),
                                             default = 'ALL',
                                             description = "Bones to consider for operator" )

    selected_bones_names = None

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT' or context.mode == 'POSE'

    #  bpy.context.selected_pose_bones
    def set_selection(self, rig):
        if self.selection_type == 'SELECTED':
            self.selected_bones_names = set([b.name for b in rig.data.bones if b.select])
            if len(self.selected_bones_names) == 0:
                self.report({'ERROR'}, "No bones selected.")
                return False
        self.selected_bones_names = None
        return True

    def is_bone_selected(self, bone_name):
        if self.selection_type == 'ALL':
            return True
        return bone_name in self.selected_bones_names

class ConstraintGameRig(GameRigOperator):
    """Constraint game rig to control rig"""
    bl_idname = "cgdive.constraint_game_rig"
    bl_label = "Constraint to control rig"
    bl_options = {'UNDO'}

    constraint_type: bpy.props.EnumProperty( name = "Constraint type",
                                             items = ( ('COPY_TRANSFORMS',  "Copy Transforms", ""),
                                                       ('COPY_LOCROT', "Copy Location-Rotation", "")
                                                      ),
                                             default = 'COPY_TRANSFORMS',
                                             description = "Type of constraints to use to constraint game rig to control rig" )

    constraint_preclear: bpy.props.BoolProperty(name="Preclear constraints", default=True)

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        layout.prop(self, "selection_type")
        layout.prop(self, "constraint_type")
        layout.prop(self, "constraint_preclear")

    def execute(self, context):
        if not self.check_rigs(context, [1,2]):
            return {'CANCELLED'}
        if not self.set_selection(context):
            return {'CANCELLED'}

        constraint_copy = None
        if self.constraint_type == 'COPY_TRANSFORMS':
            constraint_copy = utils.constraint_copy_transforms
        elif self.constraint_type == 'COPY_LOCROT':
            constraint_copy = utils.constraint_copy_locrot

        control_bnames = set( [pb.name for pb in self.control_rig.pose.bones] )
        for pb in self.game_rig.pose.bones:
            if not self.is_bone_selected(pb.name):
                continue
            if self.constraint_preclear:
                utils.remove_constraints(pb)
            if pb.name in control_bnames:
                constraint_copy(pb, self.control_rig, pb.name)

        self.report({'INFO'}, "Constraints on game rig created.")
        return {'FINISHED'}


class DisableConstraintGameRig(GameRigOperator):
    """Disable game rig constraints"""
    bl_idname = "cgdive.disable_constraint_game_rig"
    bl_label = "Disable constraints"

    def execute(self, context):
        if not self.check_rigs(context, [2]):
            return {'CANCELLED'}

        utils.set_mute_all_constraints(self.game_rig, True)

        self.report({'INFO'}, "Constraints on game rig disabled.")
        return {'FINISHED'}

class EnableConstraintGameRig(GameRigOperator):
    """Enable game rig constraints"""
    bl_idname = "cgdive.enable_constraint_game_rig"
    bl_label = "Enable constraints"

    def execute(self, context):
        if not self.check_rigs(context, [2]):
            return {'CANCELLED'}

        utils.set_mute_all_constraints(self.game_rig, False)

        self.report({'INFO'}, "Constraints on game rig enabled.")
        return {'FINISHED'}

class FlattenGameRig(GameRigOperator):
    """Flatten game rig hierarchy. Use with copy transforms constraints"""
    bl_idname = "cgdive.flatten_game_rig"
    bl_label = "Flatten hierarchy"
    bl_options = {'UNDO'}

    force_transform_constr: bpy.props.BoolProperty(name="Force Copy Transform constraints", default=True)

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        layout.prop(self, "selection_type")
        layout.prop(self, "force_transform_constr")

    def execute(self, context):
        rigs = [2]
        if self.force_transform_constr:
            rigs.append(1)
        if not self.check_rigs(context, rigs):
            return {'CANCELLED'}
        if not self.set_selection(context):
            return {'CANCELLED'}

        control_bnames = None
        if self.force_transform_constr:
            control_bnames = set( [pb.name for pb in self.control_rig.pose.bones] )

        original_mode = context.mode
        utils.make_single_active(self.game_rig, mode='EDIT')

        for eb in self.game_rig.data.edit_bones:
            if not self.is_bone_selected(eb.name):
                continue
            eb.parent = None
            if self.force_transform_constr:
                pb = self.game_rig.pose.bones[eb.name]
                utils.remove_constraints(pb)
                if eb.name in control_bnames:
                    utils.constraint_copy_transforms(pb, self.control_rig, eb.name)

        bpy.ops.object.mode_set(mode=original_mode, toggle=False)

        self.report({'INFO'}, "{} game rig bones' hierarchy flattened.".format(self.selection_type.capitalize()))
        return {'FINISHED'}

class DisconnectGameRig(GameRigOperator):
    """Disconnect game rig bones. Use with copy location-rotation constraints"""
    bl_idname = "cgdive.disconnect_game_rig"
    bl_label = "Disconnect bones"
    bl_options = {'UNDO'}

    force_locrot_constr: bpy.props.BoolProperty(name="Force Copy Loc-Rot constraints", default=True)

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        layout.prop(self, "selection_type")
        layout.prop(self, "force_locrot_constr")

    def execute(self, context):
        rigs = [2]
        if self.force_locrot_constr:
            rigs.append(1)
        if not self.check_rigs(context, rigs):
            return {'CANCELLED'}
        if not self.set_selection(context):
            return {'CANCELLED'}

        control_bnames = None
        if self.force_locrot_constr:
            control_bnames = set( [pb.name for pb in self.control_rig.pose.bones] )

        original_mode = context.mode
        utils.make_single_active(self.game_rig, mode='EDIT')

        for eb in self.game_rig.data.edit_bones:
            if not self.is_bone_selected(eb.name):
                continue
            eb.use_connect = False
            if self.force_locrot_constr:
                pb = self.game_rig.pose.bones[eb.name]
                utils.remove_constraints(pb)
                if eb.name in control_bnames:
                    utils.constraint_copy_locrot(pb, self.control_rig, eb.name)

        bpy.ops.object.mode_set(mode=original_mode, toggle=False)

        self.report({'INFO'}, "{} game rig bones disconnected.".format(self.selection_type.capitalize()))
        return {'FINISHED'}
