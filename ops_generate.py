import bpy
from . import utils

class BaseOperator(bpy.types.Operator):
    source_rig = None
    control_rig = None
    game_rig = None

    rigs = ("source_rig", "control_rig", "game_rig")

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def check_rigs(self, context, rig_ids):
        for i in rig_ids:
            rig_id = self.rigs[i]
            rig_label = rig_id.rsplit("_",1)[0]
            rig_name = getattr(context.scene.cgdive_settings, rig_id)
            if rig_name not in context.scene.objects:
                self.report({'ERROR'}, "{0} rig not found in the scene.".format(rig_label))
                return False
            rig = context.scene.objects[rig_name]
            if rig.type != 'ARMATURE':
                self.report({'ERROR'}, "{0} rig is not an armature.".format(rig_label))
                return False
            setattr(self, rig_id, rig)
        return True


class GenerateGameRig(BaseOperator):
    """Generate game rig"""
    bl_idname = "cgdive.generate_game_rig"
    bl_label = "Generate game rig"
    bl_options = {'UNDO'}

    meshes = None
    meshes_vgs_names = None

    game_rig_name: bpy.props.StringProperty( name="Rig name", default="" )

    selection_type: bpy.props.EnumProperty( name = "Selection",
                                            items = ( ('ALL',  "All", ""),
                                                      ('SELECTED', "Only selected", "")
                                                     ),
                                            default = 'ALL',
                                            description = "Bone selection for extraction" )

    extract_mode: bpy.props.EnumProperty( name= "Extract mode",
                                          items= ( ('ALL', "All", "All"),
                                                   ('DEFORM', "Deform", "Deform"),
                                                   ('VG', "Vertex groups", "Vertex groups")
                                                 ),
                                          default = 'DEFORM',
                                          description = "Game rig bone extraction mode on selection" )

    reparent: bpy.props.BoolProperty(name="Reparent meshes to game rig", default=True)
    move_to_first_layer: bpy.props.BoolProperty(name="Move bones to first layer", default=True)

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout

        self.game_rig_name = context.scene.cgdive_settings.source_rig + "_GAME"
        layout.prop(self, "game_rig_name")
        layout.prop(self, "selection_type")
        layout.prop(self, "extract_mode")
        layout.prop(self, "reparent")
        layout.prop(self, "move_to_first_layer")

    def extract_bone(self, b):
        if self.selection_type == 'SELECTED' and not b.select:
            return False

        if self.extract_mode == 'ALL':
            return True # keep all bones
        if self.extract_mode == 'DEFORM':
            return b.use_deform  # remove non-deforming bones
        if self.extract_mode == 'VG':
            return b.name.lower() in self.meshes_vgs_names  # remove bones without vertex groups
        raise ValueError("Invalid extract_mode: '{}'".format(self.extract_mode))

    def execute(self, context):
        if not self.check_rigs(context, [0]):
            return {'CANCELLED'}

        if self.reparent or self.extract_mode == 'VG':
            self.meshes = utils.get_child_meshes(self.source_rig)
        if self.extract_mode == 'VG':
            self.meshes_vgs_names = utils.get_meshes_vgs_names(self.meshes)

        self.source_rig.lock_location = (False, False, False)
        self.source_rig.location = (0,0,0)
        game_rig = utils.duplicate_rig(self.source_rig, context)  # duplicate original armature
        game_rig.name = self.game_rig_name

        game_rig.animation_data_clear()      # remove actions and drivers
        game_rig.data.animation_data_clear()

        utils.remove_custom_properties(game_rig, context.mode) # remove custom properties

        for pb in game_rig.pose.bones:
            pb.custom_shape = None  # remove custom shapes
            utils.remove_constraints(pb)  # remove constraints
            utils.unlock_all_transforms(pb)   # unlock all transforms

        bones_to_remove = []
        for b in game_rig.data.bones:
            b.bbone_segments = 1    # disable bendy bones

            b.inherit_scale = 'FULL'
            b.use_inherit_rotation = True
            b.use_local_location = True

            if not self.extract_bone(b):  # extract bones
                bones_to_remove.append(b.name)

        utils.remove_bones(game_rig, bones_to_remove, context.mode)

        if self.move_to_first_layer:
            for b in game_rig.data.bones:
                utils.move_bone_to_layer(b, 0)
            utils.only_enable_layer(game_rig, 0)

        game_rig.data.display_type = 'OCTAHEDRAL'
        game_rig.display_type = 'SOLID'

        if self.reparent:
            utils.change_meshes_armature(self.meshes, self.source_rig, game_rig)  # attach game_rig to meshes

        context.scene.cgdive_settings.game_rig = game_rig.name
        self.report({'INFO'}, "Game rig generated.")
        return {'FINISHED'}

class GenerateControlRig(BaseOperator):
    """Generate control rig"""
    bl_idname = "cgdive.generate_control_rig"
    bl_label = "Generate control rig"
    bl_options = {'UNDO'}

    control_rig_name: bpy.props.StringProperty( name="Rig name", default="" )

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        self.control_rig_name = context.scene.cgdive_settings.source_rig + "_CTRL"
        layout.prop(self, "control_rig_name")

    def execute(self, context):
        if not self.check_rigs(context, [0]):
            return {'CANCELLED'}

        self.source_rig.lock_location = (False, False, False)
        self.source_rig.location = (0,0,0)
        control_rig = utils.duplicate_rig(self.source_rig, context)  # duplicate original armature
        control_rig.name = self.control_rig_name

        for b in control_rig.data.bones:
            b.use_deform = False  # set all bones to non-deform
            b.bbone_segments = 1 # disable bendy bones

        context.scene.cgdive_settings.control_rig = control_rig.name
        self.report({'INFO'}, "Control rig generated.")
        return {'FINISHED'}
