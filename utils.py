import bpy

def make_single_active(ob, mode):
    if mode != 'OBJECT':
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
    bpy.ops.object.select_all(action='DESELECT')
    ob.select_set(True)
    bpy.context.view_layer.objects.active = ob
    if mode != 'OBJECT':
        bpy.ops.object.mode_set(mode=mode, toggle=False)

def duplicate_rig(rig, context):
    make_single_active(rig, mode=context.mode)
    bpy.ops.object.duplicate(linked=False)
    return context.selected_objects[0]

def move_bone_to_layer(b, n):
    for i in range(0, len(b.layers)):
        b.layers[i] = (i == n)

def only_enable_layer(rig, n):
    for i in range(0, len(rig.data.layers)):
        rig.data.layers[i] = (i == n)

def get_child_meshes(rig):
    obs = [rig]
    meshes = []

    while len(obs) > 0:
        ob = obs.pop()
        if ob.type == 'MESH':
            meshes.append(ob)
        for c in ob.children:
            obs.append(c)

    return meshes

def change_meshes_armature(meshes, src_rig, new_rig):
    for mesh in meshes:
        for m in mesh.modifiers:
            if m.type == 'ARMATURE' and m.object == src_rig:
                m.object = new_rig

    src_children = [c for c in src_rig.children]
    for c in src_children:
        c.parent = new_rig
        c.matrix_parent_inverse = new_rig.matrix_world.inverted()

def get_meshes_vgs_names(meshes, lowercase=True):
    vgs_names = set()
    for m in meshes:
        for vg in m.vertex_groups:
            if lowercase:
                vgs_names.add(vg.name.lower())
            else:
                vgs_names.add(vg.name)
    return vgs_names

def remove_bones(rig, bones_names, mode):
    make_single_active(rig, mode='EDIT')
    for bn in bones_names:
        rig.data.edit_bones.remove( rig.data.edit_bones[bn] )
    bpy.ops.object.mode_set(mode=mode, toggle=False)

def remove_constraints(pb):
    csts = [c for c in pb.constraints]
    for c in csts:
        pb.constraints.remove(c)

def set_mute_all_constraints(rig, value):
    for pb in rig.pose.bones:
        for c in pb.constraints:
            c.mute = value

def unlock_all_transforms(pb):
    pb.lock_location = (False, False, False)
    pb.lock_rotation_w = False
    pb.lock_rotation = (False, False, False)
    pb.lock_scale = (False, False, False)

def constraint_copy_transforms(pb, target, subtarget):
    c = pb.constraints.new('COPY_TRANSFORMS')
    c.target = target
    c.subtarget = subtarget

def constraint_copy_locrot(pb, target, subtarget):
    c = pb.constraints.new('COPY_LOCATION')
    c.target = target
    c.subtarget = subtarget
    c = pb.constraints.new('COPY_ROTATION')
    c.target = target
    c.subtarget = subtarget

def remove_custom_properties(rig, mode):
    def delete_properties(datablock):
        rna_ui = datablock.get("_RNA_UI", None)
        if rna_ui is not None:
            for prop in rna_ui:
                del datablock[prop]

    delete_properties(rig)
    delete_properties(rig.data)

    for pb in rig.pose.bones:
        delete_properties(pb)

    make_single_active(rig, 'EDIT')
    for eb in rig.data.edit_bones:
        delete_properties(eb)
    bpy.ops.object.mode_set(mode=mode, toggle=False)

def get_rig_actions(rig, only_nla=False):
    actions = set()
    if rig.animation_data:
        if not only_nla and rig.animation_data.action is not None:
            actions.add(rig.animation_data.action.name)
        for nla_track in rig.animation_data.nla_tracks:
            for s in nla_track.strips:
                actions.add(s.action.name)
    return actions

def nla_tracks_set(rig, solo=None, mute=None):
    if solo is None and mute is None:
        raise ValueError("Must set at least one parameter.")
    if rig.animation_data:
        for track in rig.animation_data.nla_tracks:
            if solo is not None:
                track.is_solo = solo
            if mute is not None:
                track.mute = mute

def get_active_action(rig):
    if rig.animation_data:
        return rig.animation_data.action
    return None

def set_active_action(rig, acc):
    if rig.animation_data:
        rig.animation_data.action = acc
